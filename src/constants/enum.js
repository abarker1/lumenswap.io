export const trsStatus = {
  SUCCESS: 'success',
  FAIL: 'fail',
  WARNING: 'WARNING',
};

export const connectModalTab = {
  CONNECT: 'connect',
  PUBLIC: 'public',
  PRIVATE: 'private',
};
