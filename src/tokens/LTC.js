import logo from 'src/assets/images/ltc.png';

export default {
  code: 'LTC',
  logo,
  web: 'apay.io',
  issuer: 'GC5LOR3BK6KIOK7GKAUD5EGHQCMFOGHJTC7I3ELB66PTDFXORC2VM5LP',
};
