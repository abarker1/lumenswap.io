import logo from 'src/assets/images/mobi.png';

export default {
  code: 'MOBI',
  logo,
  web: 'mobius.network',
  issuer: 'GA6HCMBLTZS5VYYBCATRBRZ3BZJMAFUDKYYF6AH6MVCMGWMRDNSWJPIH',
};
