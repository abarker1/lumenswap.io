import logo from 'src/assets/images/usdx.png';

export default {
  code: 'USD',
  logo,
  web: 'anchorusd.com',
  issuer: 'GDUKMGUGDZQK6YHYA5Z6AY2G4XDSZPSZ3SW5UN3ARVMO6QSRDWP5YLEX',
};
