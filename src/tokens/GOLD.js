import logo from 'src/assets/images/gold.png';

export default {
  code: 'GOLD',
  logo,
  web: 'www.mintx.com',
  issuer: 'GBC5ZGK6MQU3XG5Y72SXPA7P5R5NHYT2475SNEJB2U3EQ6J56QLVGOLD',
};
